// Load jQuery
$ = jQuery.noConflict();

$(document).ready(function() {
  $('.mobile-menu a').on('click', function() {
    $('nav.site-nav').toggle('slow');
  });

  let breakpoint = 768;
  $(window).resize(function() {
    if($(document).width() >= breakpoint) {
      $('nav.site-nav').show();
    } else {
      $('nav.site-nav').hide();
    }
  });


  // Fluidbox Plugin
  // Added data-fluidbox attribute
  jQuery('.gallery a').each(function() {
    jQuery(this).attr({'data-fluidbox': ''});
  });

  if(jQuery('[data-fluidbox]').length > 0) {
    jQuery('[data-fluidbox]').fluidbox();
  }
}); 


// let nav = document.querySelector('.mobile-menu a');
// nav.addEventListener('click', function() {
//   let siteNav = document.querySelector('nav.site-nav');
//   siteNav.style.display = 'block';
// });










