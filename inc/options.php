<?php
// Add Options to the Dashboard
function lapizzeria_options() {
  add_menu_page('La Pizzeria', 'La Pizzeria Options', 'administrator', 'lapizzeria_options', 'lapizzeria_adjusments', '', 20); // '' is the icon, 20 is the position
  add_submenu_page('lapizzeria_options', 'Reservations', 'Reservations', 'administrator', 'lapizzeria_reservations', 'lapizzeria_reservations');
}
add_action('admin_menu', 'lapizzeria_options');

// Additional options to Dashboard
function lapizzeria_settings() {
  // Google Maps Group
  // register_setting('lapizzeria_options_gmaps', 'latitude');

  // Information Group
  register_setting('lapizzeria_options_info', 'lapizzeria_location'); 
  register_setting('lapizzeria_options_info', 'lapizzeria_phonenumber'); 
}
add_action('init', 'lapizzeria_settings');

function lapizzeria_adjusments() { ?>
  <div class="wrap">
    <h1>La Pizzeria Adjusments</h1>
    <form action="options.php" method="post">
    


      <!-- Add address and phone number -->
      <?php
        settings_fields('lapizzeria_options_info');
        do_settings_sections('lapizzeria_options_info');
      ?>
      <h2>Other Adjusments</h2>
      <table class="form-table">
        <tr valign="top">
          <th scope="row">Address: </th>
          <td>
            <input type="text" name="lapizzeria_location" value="<?php echo esc_attr( get_option('lapizzeria_location') ); ?>" >
          </td>
        </tr>

        <tr valign="top">
          <th scope="row">Phone number: </th>
          <td>
            <input type="text" name="lapizzeria_phonenumber" value="<?php echo esc_attr( get_option('lapizzeria_phonenumber') ); ?>" >
          </td>
        </tr>
      </table>
      <?php submit_button(); ?>
    </form>
  </div>
<?php }

function lapizzeria_reservations() { ?>
  <!-- wrap is a built in class for wordpress backend -->
  <div class="wrap">
    <table class="wp-list-table widefat striped">
      <thead>
        <tr>
          <th class="manage-column">ID</th>
          <th class="manage-column">Name</th>
          <th class="manage-column">Date of Reservation</th>
          <th class="manage-column">Email</th>
          <th class="manage-column">Phone Number</th>
          <th class="manage-column">Message</th>
        </tr>
      </thead>
      <tbody>
        <?php
          // Get the reservation data from mysql 
          global $wpdb;
          $table = $wpdb->prefix . 'reservations';
          // Query only for custom table
          $reservations = $wpdb->get_results("SELECT * FROM $table", ARRAY_A);
          // echo "<pre>";
          // var_dump($reservations);
          // echo "</pre>";

          // Check and get the key from the associative array (use var_dump)
          foreach($reservations as $reservation): ?>
            <tr>
              <td><?php echo $reservation['id']; ?></td>
              <td><?php echo $reservation['name']; ?></td>
              <td><?php echo $reservation['date']; ?></td>
              <td><?php echo $reservation['email']; ?></td>
              <td><?php echo $reservation['phone']; ?></td>
              <td><?php echo $reservation['message']; ?></td>
            </tr>
          <?php endforeach; ?>
        ?>
      </tbody>
    </table>
  </div>


  <?php }


?>