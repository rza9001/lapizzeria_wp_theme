<?php
  function lapizzeria_save_reservation() {
    global $wpdb;

    // Check if the user actually input the form
    if(isset($_POST['reservation']) && $_POST['hidden'] == '1') {
      // Read the value from the form name
      $name = sanitize_text_field($_POST['name']);
      $date = sanitize_text_field($_POST['date']);
      $email = sanitize_email($_POST['email']);
      $phone = sanitize_text_field($_POST['phone']);
      $message = sanitize_text_field($_POST['message']);

      // Read the db prefix and insert the data to reservations table
      $table = $wpdb->prefix . 'reservations';

      // Set the value for the db table
      $data = array(
        'name' => $name,
        'date' => $date,
        'email' => $email,
        'phone' => $phone,
        'message' => $message
      );

      $format = array(
        '%s',
        '%s',
        '%s',
        '%s',
        '%s'
      );

      // Insert data into custom wordpress table
      $wpdb->insert($table, $data, $format);

      $url = get_page_by_title('Thanks for your reservation!'); // Thanks for your reservation! : judul dari sebuah page
      wp_redirect(get_permalink($url));
      exit();
    }
  }
add_action('init', 'lapizzeria_save_reservation');  

?>