<?php
  get_header();
?>
  
  <?php while(have_posts()): the_post(); ?>
    <div class="hero" style='background-image:url(<?php echo get_the_post_thumbnail_url(); ?>);'>
      <div class="hero-content">
        <div class="hero-text">
          <h2><?php the_title(); ?></h2>
        </div>
      </div>
    </div>

    <div class="main-content container">
      <main class="text-center content-text">
        <?php the_content() ?>
      </main>
    </div>

    <!-- Show the post thumbnail -->
    <?php // the_post_thumbnail(); ?>
    
    <div class="box-information container">
      <div class="single-box">
        <?php
          // Get image id from ACF plugin based on the ID
          $id_image = get_field('image_1');
          $image = wp_get_attachment_image_src($id_image, 'boxes'); // boxes: ukuran image dari functions.php
        ?>

        <img src="<?php echo $image[0];  // $image[0]: url ke img dgn size boxes. Cek pake var_dump ?>" alt="image 1">
        <div class="content-box">
          <?php the_field('description_1'); ?>
        </div>
      </div>
      <div class="single-box">
        <?php
          // Get image id from ACF plugin based on the ID
          $id_image = get_field('image_2');
          $image = wp_get_attachment_image_src($id_image, 'boxes'); // boxes: ukuran image dari functions.php
        ?>

        <img src="<?php echo $image[0];  // $image[0]: url ke img dgn size boxes. Cek pake var_dump ?>" alt="image 2">
        <div class="content-box">
          <?php the_field('description_2'); ?>
        </div>
      </div>
      <div class="single-box">
        <?php
          // Get image id from ACF plugin based on the ID
          $id_image = get_field('image_3');
          $image = wp_get_attachment_image_src($id_image, 'boxes'); // boxes: ukuran image dari functions.php
        ?>

        <img src="<?php echo $image[0];  // $image[0]: url ke img dgn size boxes. Cek pake var_dump ?>" alt="image 3">
        <div class="content-box">
          <?php the_field('description_3'); ?>
        </div>
      </div>
    </div>

  <?php endwhile; ?>

<?php
  get_footer();
?>

