<footer>
  <?php
    // Print the header menu
    $args = array(
    'theme_location' => 'header-menu',  // 'header-menu' dari function.php
    'container' => 'nav',
    'after' => '<span class="separator"> | </span>'
    );
    wp_nav_menu($args);
  ?>

  <div class="location">
    <!-- Get the value from options.php -->
    <p><?php echo esc_html(get_option('lapizzeria_location')); ?></p>
    <p>Phone Number: <?php echo esc_html(get_option('lapizzeria_phonenumber')); ?></p>
  </div>

  <p class="copyright">All rights reserved <?php echo date('Y'); ?></p>
</footer>

  <?php wp_footer(); // hook the javascript and menampilkan bar ?>  
 </body>
</html>