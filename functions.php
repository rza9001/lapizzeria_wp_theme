<?php

// Import the database.php file from inc folder
require get_template_directory() . '/inc/database.php';

// Handles the reservation form submission to the database
require get_template_directory() . '/inc/reservations.php';

// Import the options file from inc folder
// Creates options page for the theme
require get_template_directory() . '/inc/options.php';




function lapizzeria_setup() {
  // Add feature image to the post page
  add_theme_support('post-thumbnails');

  // Add image sizes
  add_image_size('boxes', 437, 291, true);
	add_image_size('specialties', 768, 515, true);
	add_image_size('specialty-portrait', 435, 530, true);  // for front-page
	
	// Update the thumbnail size on Media settings
	 update_option('thumbnail_size_w', 253);
	 update_option('thumbnail_size_h', 164);

}
add_Action('after_setup_theme', 'lapizzeria_setup');

// Add CSS and JS
function lapizzeria_styles() {
  // Adding stylesheets
  wp_register_style('googlefont', 'https://fonts.googleapis.com/css?family=Open+Sans:400,700|Raleway:400,700,900&display=swap', array(), '1.0.0');
  wp_register_style('normalize', get_template_directory_uri(). '/css/normalize.css', array(), '6.0.0');
  wp_register_style('fluidboxcss', get_template_directory_uri(). '/css/fluidbox.min.css', array(), '1.0.0');
  wp_register_style('fontawesome', get_template_directory_uri(). '/css/font-awesome.css', array(), '4.7.0');
  wp_register_style('datetime-local', get_template_directory_uri(). '/css/datetime-local-polyfill.css', array(), '1.0.0');
  wp_register_style('style', get_template_directory_uri(). '/style.css', array('normalize'), '1.0');

  // enqueue the style
  wp_enqueue_style('normalize');
  wp_enqueue_style('fluidboxcss');
  wp_enqueue_style('fontawesome');
  wp_enqueue_style('datetime-local');
  wp_enqueue_style('googlefont');
  wp_enqueue_style('style');

	// Add JS files
	wp_register_script('fluidboxjs', get_template_directory_uri() . '/js/jquery.fluidbox.min.js', array('jquery'), '1.0.0', true);
	wp_register_script('debounce', '//cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js', array('jquery'), '1.0.0', true);
	wp_register_script('datetime-local-polyfill', get_template_directory_uri() . '/js/datetime-local-polyfill.min.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker', 'modernizr' ), '1.0.0', true);

	wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3', true );
  wp_register_script('script', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true); // true: script di add di bagian bawah 

  // enqueue the scripts
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-ui-core');  // Already included in Wordpress
	wp_enqueue_script('jquery-ui-datepicker'); // Already included in Wordpress
	wp_enqueue_script('datetime-local-polyfill');
	wp_enqueue_script('debounce');
	wp_enqueue_script('modernizr');
  wp_enqueue_script('fluidboxjs');
  wp_enqueue_script('script');
}
add_action('wp_enqueue_scripts', 'lapizzeria_styles');  // Jangan lupa tambahkan wp_head() di header.php 



// Add Menus
function lapizeria_menus() {
  register_nav_menus(array(
    'header-menu' => __('Header Menu', 'lapizzeria'), // lapizzeria = text domain
    'social-menu' => __('Social Menu', 'lapizzeria')
  ));
}
add_action('init', 'lapizeria_menus');



// Custom Post Type (CPT)
function lapizzeria_specialties() {
	$labels = array(
		'name'               => _x( 'Pizzas', 'lapizzeria' ),
		'singular_name'      => _x( 'Pizza', 'post type singular name', 'lapizzeria' ),
		'menu_name'          => _x( 'Pizzas', 'admin menu', 'lapizzeria' ),
		'name_admin_bar'     => _x( 'Pizzas', 'add new on admin bar', 'lapizzeria' ),
		'add_new'            => _x( 'Add New', 'book', 'lapizzeria' ),
		'add_new_item'       => __( 'Add New Pizza', 'lapizzeria' ),
		'new_item'           => __( 'New Pizzas', 'lapizzeria' ),
		'edit_item'          => __( 'Edit Pizzas', 'lapizzeria' ),
		'view_item'          => __( 'View Pizzas', 'lapizzeria' ),
		'all_items'          => __( 'All Pizzas', 'lapizzeria' ),
		'search_items'       => __( 'Search Pizzas', 'lapizzeria' ),
		'parent_item_colon'  => __( 'Parent Pizzas:', 'lapizzeria' ),
		'not_found'          => __( 'No Pizzas found.', 'lapizzeria' ),
		'not_found_in_trash' => __( 'No Pizzas found in Trash.', 'lapizzeria' )
	);

	$args = array(
		'labels'             => $labels,
    'description'        => __( 'Description.', 'lapizzeria' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'specialties' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 6,
		'supports'           => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'          => array( 'category' ),
	);

	register_post_type( 'specialties', $args );
}

add_action( 'init', 'lapizzeria_specialties' );



// Add widget zone
function lapizzeria_widgets() {
	register_sidebar(array(
		'name' => 'Blog Sidebar',
		'id' => 'blog_sidebar',
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}
add_action('widgets_init', 'lapizzeria_widgets');
















