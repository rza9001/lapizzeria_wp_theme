<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Make this iOS compatible -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title"  content="La Pizzeria Restaurant">
  <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() ?>/apple-touch-icon.jpg">

    <!-- Make this Android compatible -->
    <meta name="theme-color" content="#a61206"> <!-- Change the browser top bar color -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="La Pizzeria Restaurant">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() ?>/icon.png" sizes="192x192">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>La Pizzeria</title>

  <!-- Function to add stylesheet to wordpress -->
  <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<header class="site-header">
  <div class="container">
    <!-- Logo -->
    <div class="logo">
      <a href="<?php echo esc_url(home_url('/')); ?>">  <!-- url to home page -->
        <img src="<?php echo get_template_directory_uri() ?>/img/logo.svg" class="logoimage" alt="Logo">
      </a>
    </div>
    <!-- End Logo -->

    <div class="header-information">
      <!-- Socials menu -->
      <div class="socials">
        <?php
          $args = array(
            'theme_location'  => 'social-menu',      // Tell wordpress which menu will print
            'container'       => 'nav',
            'container_class' => 'socials',
            'container_id'    => 'socials',
            'link_before'     => '<span class="sr-text">',
            'link_after'      => '</span>'
          );
          wp_nav_menu($args);
        ?>
      </div>
      <div class="address">
        <!-- Get the value from options.php -->
        <p><?php echo esc_html(get_option('lapizzeria_location')); ?></p>
        <p>Phone Number: <?php echo esc_html(get_option('lapizzeria_phonenumber')); ?></p>
      </div>
    </div>
  </div> 
  <!-- End container -->
</header>


<!-- Add menu -->
<div class="main-menu">
  <div class="mobile-menu">
	 <a href="#" class="mobile"><i class="fa fa-bars"></i> Menu</a>
  </div>

  <div class="navigation container">
    <?php
      $args = array(
        'theme_location' => 'header-menu',      // Tell wordpress which menu will print
        'container' => 'nav',
        'container_class' => 'site-nav'
      );
      wp_nav_menu($args);
    ?>
  </div>
</div>

